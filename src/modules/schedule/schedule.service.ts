import {Injectable} from '@nestjs/common';
import {IScheduleItem} from "./types";
import axios from "axios";
import * as cheerio from "cheerio";
import {CourseSeason, EducationLevel} from "../../common/types";

@Injectable()
export class ScheduleService {
    async getScheduleData(year: number, season: string): Promise<IScheduleItem[]> {
        let seasonNumber: number;
        let seasonValue: CourseSeason;
        if (season == 'autumn') {
            seasonNumber = 1;
            seasonValue = CourseSeason.AUTUMN;
        } else if (season == 'spring') {
            seasonNumber = 2;
            seasonValue = CourseSeason.SPRING;
            year--;
        } else {
            seasonNumber = 3
            seasonValue = CourseSeason.SUMMER;
            year--;
        }

        const response = await axios.get(`https://my.ukma.edu.ua/schedule/?year=${year}&season=${seasonNumber}`);
        const $ = cheerio.load(response.data);
        let result: IScheduleItem[] = [];

        let facultiesList = $('#schedule-accordion .panel-info');
        facultiesList.each((_number, element) => {
            let facultyName = $(element).find('.panel-heading > h4 > a')
                .first()
                .text()
                .replace(/[\n]/g, '')
                .trim();

            let parentData = $(element).find('.panel-collapse > div > div').children();
            parentData.each((_number, element) => {
                let levelText = $(element).find('div > h4 > a')
                    .first()
                    .text()
                    .split(', ')[0]
                    .trim();

                let levelNumber = Number($(element).find('div > h4 > a')
                    .first()
                    .text()
                    .split(', ')[1]
                    .trim()[0]);

                let level = (levelText === 'БП') ? EducationLevel.BACHELOR: EducationLevel.MASTER;
                let childrenData = $(element).find('.panel-collapse > .list-group').children();
                childrenData.each((_number, element) => {
                    let url = String($(element).find('div > a')
                        .last()
                        .attr('href'));

                    let specialityName = $(element).find('div > a')
                        .last()
                        .text()
                        .trim()
                        .split(levelText)[0]
                        .trim();

                    let updateData = $(element).find('div > span')
                        .first()
                        .text()
                        .split(' ');

                    let date = updateData[1].split('.');
                    let time = updateData[2].slice(0, -1);
                    let updatedAt = `${date[2]}-${date[1]}-${date[0]} ${time}`;

                    /*
                      facultyName: string;
                      specialityName: string;
                      level: EducationLevel;
                      year: 1 | 2 | 3 | 4;
                      season: CourseSeason;
                    */

                    result.push({
                        url: url,
                        updatedAt: updatedAt,
                        facultyName: facultyName,
                        specialityName: specialityName,
                        level: level,
                        year: (levelNumber == 1) ? 1: (levelNumber == 2) ? 2: (levelNumber == 3) ? 3: 4,
                        season: seasonValue
                    });
                });
            });
        });
        return result;
         // return response.data;
    }
}

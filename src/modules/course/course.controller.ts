import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param
} from '@nestjs/common';
import { CourseService } from './course.service';

@Controller('course')
export class CourseController {
  constructor(
    protected readonly service: CourseService,
  ) {}

  @Get(':code')
  public async getCourseData(@Param('code') courseCode: number): Promise<unknown> {
    require('https').globalAgent.options.rejectUnauthorized = false;
    return this.service.getCourseData(courseCode).catch(error => {
      const exception = error?.response?.status;
      if (exception == 404) {
        throw new NotFoundException('Not found');
      } else if (exception == 400){
        throw new BadRequestException('Inserted values are incorrect');
      } else if (exception == 500){
        throw new InternalServerErrorException('Failed to connect to server');
      }
    });
  }
}

import {Injectable} from '@nestjs/common';
import {ICourse} from './types';
import axios from 'axios';
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from '../../common/types';

@Injectable()
export class CourseService {
    async getCourseData(courseCode: number): Promise<ICourse> {
        const response = await axios.get(`https://my.ukma.edu.ua/course/${courseCode}`);

        const $ = cheerio.load(response.data);

        let code = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(1) > td').text());

        let name = ($('div.container > ul > li:nth-child(3)')).text();

        let description = ($('#course-card--' + courseCode + '--info')
            .text()
            .replace(/[\n\t]/g, ''));

        let facultyName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(3) > td').text());

        let departmentName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(4) > td').text());

        let levelText = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(5) > td').text());

        let level = (levelText === 'Бакалавр') ? EducationLevel.BACHELOR: EducationLevel.MASTER;

        let year = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(3)')
            .text().split(' ')[0]);

        let seasons: CourseSeason[] = []
        $('#w0 > table > tbody:nth-child(2)').each((_number, element) => {
            const temp = $(element).find("th").text().split('\t');
            temp.forEach(t => {
                if (t == 'Осінь') {
                    seasons.push(CourseSeason.SPRING);
                } else if (t == 'Весна') {
                    seasons.push(CourseSeason.AUTUMN);
                } else if (t == 'Літо'){
                    seasons.push(CourseSeason.SUMMER);
                }
            });
        });

        let creditsAmount = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(1)')
            .text()
            .split(' ')[0]);

        let hoursAmount = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(2)')
            .text()
            .split(' ')[0]);

        let teacherName = ($('#w0 > table > tbody:nth-child(1) > tr:nth-child(7) > th').next().text());

        return {
            code: code,
            name: name,
            description: description,
            facultyName: facultyName,  // Назва факультету
            departmentName: departmentName, // Назва кафедри
            level: level,
            year: (year == 1) ? 1: (year == 2) ? 2: (year == 3) ? 3: 4,
            seasons: seasons,
            creditsAmount: creditsAmount,
            hoursAmount: hoursAmount,
            teacherName: teacherName
        };


        //return response.data;
    }
}
